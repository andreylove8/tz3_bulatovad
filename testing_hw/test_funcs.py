from my_code import my_sum, my_min, my_mult, my_max


def test_speed():
    import timeit
    # if it needs to use pytest for the second time, the bsize_file.txt will have x2 amount of numbers (2 triplets),
    # so it'll be correct to delete this file or make it empty
    res2 = timeit.timeit("count_funcs2()", setup="from my_code import count_funcs2", number=1)
    res1 = timeit.timeit("count_funcs1()", setup="from my_code import count_funcs1", number=1)
    assert res1 < res2


def test_my_sum():
    assert my_sum([1, 2, 3]) == 6
    assert my_sum([-1, 1, 0]) == 0


def test_my_mult():
    assert my_mult([1, 2, 3]) == 6
    assert my_mult([1, 0, 2]) == 0
    assert my_mult([2, -1, 5]) == -10


def test_my_min():
    assert my_min([1, 2, -1]) == -1
    assert my_min([1, 2, 3]) == 1
    assert my_min([0, 1, 9]) == 0


def test_my_max():
    assert my_max([1, 2, -1]) == 2
    assert my_max([1, 2, 3]) == 3
    assert my_max([0, 1, 9]) == 9

def except_error(function_n, *args, **kwargs):
    try:
        function_n(args, kwargs)
        return False
    except:
        return True


def test_empty_sets():
    assert my_sum([]) == 0  
    assert my_mult([]) == 1  
    assert except_error(my_min, [])
    assert except_error(my_max, [])
