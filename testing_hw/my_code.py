def my_sum(lst):
    curr_sum = 0
    for i in lst:
        curr_sum = int(i) + curr_sum
    return curr_sum


def my_mult(lst):
    """
       returns multiply of all values in `data`, if `data` is empty returns 1, if any error occurs returns -1 instead.
       in fact,we won't have integer overflow, because python integers have arbitrary precision,
       but if it happens, we catch it and return -1 (that means that program won't crash due to overflow)
       """
    curr_multi = 1
    try:
        for i in lst:
            curr_multi *= i
    except:
        return -1
    return curr_multi


def my_max(lst):
    cur_max = int(lst[0])
    for i in lst:
        cur_max = max(cur_max, int(i))
    return cur_max


def my_min(lst):
    cur_min = int(lst[0])
    for i in lst:
        cur_min = min(cur_min, int(i))
    return cur_min


file_name1 = "nums.hw"


def count_funcs1():
    temp_arr = []
    with open(file_name1, 'r') as f:
        for line in f:
            for i in line.split():
                temp_arr.append(int(i))
    ans_max = my_max(temp_arr)
    ans_min = my_min(temp_arr)
    ans_sum = my_sum(temp_arr)
    ans_multi = my_mult(temp_arr)
    return ans_max, ans_multi, ans_min, ans_sum, temp_arr


return1 = count_funcs1()
arr = return1[-1][:]

count_funcs1()
x = 0
file_name2 = '../bsize_file.txt'

while x < 3 * len(arr):
    with open('../bsize_file.txt', 'a') as file:
        for i in arr:
            file.write(str(i) + " ")
            x += 1


def count_funcs2():
    new_arr2 = []
    with open('../bsize_file.txt', 'r') as f:
        for line in f:
            for i in line.split():
                new_arr2.append(int(i))

    ans_max_new = my_max(new_arr2)
    ans_min_new = my_min(new_arr2)
    ans_sum_new = my_sum(new_arr2)
    ans_multi_new = my_mult(new_arr2)
    return ans_max_new, ans_multi_new, ans_sum_new, ans_min_new

def pretty_print(source, results):
    print("\n".join(map(lambda line: line[0] + ':\t' + str(line[1]), zip(["max", "mul", "min", "sum"], results))))


if __name__ == "__main__":
    pretty_print("nums.hw", count_funcs1())
